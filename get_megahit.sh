#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

#project="megahit"
#program=`echo ${project} | tr '[:upper:]' '[:lower:]'`
prog="megahit"


mkdir -p ${HOME}/src
(
cd ${HOME}/src

if [ ! -e ${prog} ]
then
    git clone https://github.com/voutcn/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi


# Use native processor architecture for compiling 
sed -i 's|^CXXFLAGS = \(.*\)$|CXXFLAGS = -march=native \1|' Makefile
## Add the path to possible locally-compiled included libraries
#sed -i 's|^EXTRA_FLAGS= \(.*\)$|EXTRA_FLAGS= -Wl,-rpath,${HOME}/lib \1|' Makefile
#sed -i 's|^INCLUDES= \(.*\)$|INCLUDES= -I${HOME}/include \1|' Makefile
#sed -i 's|^LIBPATH= \(.*\)$|LIBPATH= -L${HOME}/lib \1|' Makefile


make || "${prog} build failed"

ln -sf ${HOME}/src/${prog}/${prog} ${HOME}/bin/${prog} || error_exit "${prog} install failed"


version=$(git log | head -1)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
