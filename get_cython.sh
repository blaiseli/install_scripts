#!/usr/bin/env bash
# To use after get_samtools.sh so that the programs can use the locally-compiled zlib
# Also depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src
(
cd ${HOME}/src

prog="cython"
progname="Cython"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="0.22.1" get_cython.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="0.24.1"
fi

rm -rf ${progname}-${version}*
wget --continue --no-check-certificate https://files.pythonhosted.org/packages/source/C/${progname}/${progname}-${version}.tar.gz || error_exit "${prog} download failed"
#wget --continue http://${prog}.org/release/${progname}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${progname}-${version}.tar.gz
cd ${progname}-${version}
#CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${prog} build failed"
#python setup.py install --user || error_exit "${prog} install failed"
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python3 setup.py build || error_exit "${prog} build failed"
python3 setup.py install --user || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
