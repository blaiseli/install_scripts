#!/usr/bin/env bash
# The compilation of tophat depends on libboost:
# sudo apt-get install libboost-all-dev

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


prog="tophat"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.1.0" get_tophat.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.1.0"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue https://ccb.jhu.edu/software/${prog}/downloads/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
