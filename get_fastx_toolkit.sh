#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}



mkdir -p ${HOME}/src
cd ${HOME}/src

prog="libgtextutils"

if [ ! -e ${prog} ]
then
    git clone https://github.com/agordon/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

# Generate a configure script
autoreconf -i || error_exit "${prog} autoreconf failed"
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
# (not sure it works)

./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"

prog="fastx_toolkit"

if [ ! -e ${prog} ]
then
    git clone https://github.com/agordon/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

# Generate a configure script
autoreconf -i || error_exit "${prog} autoreconf failed"
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
# (not sure it works)
./configure --prefix=${HOME} PKG_CONFIG_PATH=${HOME}/lib/pkgconfig:${PKG_CONFIG_PATH} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"

#ln -sf ${HOME}/src/${prog}/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"

exit 0
