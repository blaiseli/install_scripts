#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="asplice"
mkdir -p ${HOME}/src/${prog}
(
cd ${HOME}/src/${prog}
# In case there are already things, remove them
rm -rf *
# Download the files
for filename in "extractfa" "extractfa2" "asplicek" "asplicel"
do
    wget --continue "http://faculty.cse.tamu.edu/shsze/asplice/${filename}.c" || error_exit "${filename} download failed"
done

CFLAGS="-O3 -march=native -fomit-frame-pointer"
# Compile them
for filename in "extractfa" "extractfa2" "asplicek" "asplicel"
do
    gcc ${CFLAGS} -o ${filename} ${filename}.c || error_exit "${filename} build failed"
done

# "install" them
for filename in "extractfa" "extractfa2" "asplicek" "asplicel"
do
    ln -sf ${HOME}/src/${prog}/${filename} ${HOME}/bin/. || error_exit "${filename} install failed"
done
)

exit 0
