#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="piPipes"
genome="dm3"
mkdir -p ${HOME}/src

(
cd ${HOME}/src

if [ ! -e ${prog} ]
then
    git clone https://github.com/bowhan/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
    ln -s ${PWD}/${prog} ${HOME}/bin/${prog}
    echo -e "1\n1\n1\n6\n21\n21\n23\n30\n" | piPipes install -g ${genome} -c $(nproc)
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
    ln -sf ${PWD}/${prog} ${HOME}/bin/${prog}
fi


version=$(git log | head -1)
)


if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
