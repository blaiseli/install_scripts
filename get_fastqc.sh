#!/usr/bin/env bash
# Depends on java
#sudo apt-get install default-jre
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="fastqc"
prog_dir="FastQC"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="0.11.5" get_fastqc.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="0.11.5"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog_dir}
rm -rf ${prog}_v${version}.zip
# Download it
wget --continue http://www.bioinformatics.babraham.ac.uk/projects/${prog}/${prog}_v${version}.zip || error_exit "${prog} download failed"
unzip ${prog}_v${version}.zip
cd ${prog_dir}
chmod +x ${prog}
# Force link re-creation if they already exist
ln -sf ${HOME}/src/${prog_dir}/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
