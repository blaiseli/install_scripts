#!/usr/bin/env bash
# sudo apt install libtbb-dev

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="bowtie2"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.2.7" get_bowtie2.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.2.4"
fi
optimize="True"

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue http://downloads.sourceforge.net/project/bowtie-bio/${prog}/${version}/${prog}-${version}-source.zip || error_exit "${prog} download failed"
unzip ${prog}-${version}-source.zip
cd ${prog}-${version}
if [ "${optimize}" == "True" ]
then
    mv Makefile Makefile.original
    # Use native processor architecture for compiling 
    sed 's/^RELEASE_FLAGS\(.*\)$/#RELEASE_FLAGS\1\nRELEASE_FLAGS = -O3 -march=native -fomit-frame-pointer -funroll-loops -g3/g' Makefile.original > Makefile.tmp
    # Remove the rule that sets clang as the compiler
    sed -e '/^ifneq.*shell uname -r/,+4d' Makefile.tmp > Makefile
    rm Makefile.tmp
fi
make || error_exit "${prog} build failed"
# Install in home directory
mkdir -p ${HOME}/bin
for binary in "${prog}" "${prog}-align-s" "${prog}-align-l" "${prog}-build" "${prog}-build-s" "${prog}-build-l" "${prog}-inspect" "${prog}-inspect-s" "${prog}-inspect-l"
do
    # Force link re-creation if they already exist
    ln -sf ${HOME}/src/${prog}-${version}/${binary} ${HOME}/bin/. || error_exit "${binary} install failed"
done
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
