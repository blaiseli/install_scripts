#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


prog="bedtools"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.25.0" get_bedtools.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.26.0"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src

if [ "${version}" == "git" ]
then
    if [ ! -e "${prog}2" ]
    then
        git clone https://github.com/arq5x/${prog}2.git || error_exit "${prog} download failed"
        cd "${prog}2"
        version=$(git log | head -1)
    else
        cd "${prog}2"
        # Stash away modified Makefile, otherwise pull may fail
        git stash
        git pull || error_exit "${prog} update failed"
        version=$(git log | head -1)
    fi
    SOURCE="${HOME}/src/${prog}2"
else
    rm -rf v${version}*
    rm -rf ${prog}2-${version}*
    wget --continue https://github.com/arq5x/${prog}2/archive/v${version}.tar.gz || error_exit "${prog} download failed"
    tar -xvzf v${version}.tar.gz
    SOURCE="${HOME}/src/${prog}2-${version}"
fi

cd ${SOURCE}
# Doesn't work with version 2.25 ?
# Use native processor architecture for compiling
# "ERROR: LeakSanitizer: detected memory leaks" when using version compiled with -fsanitize=address
#sed -i 's|^export CXXFLAGS =\(.*\) \$(INCLUDES)$|export CXXFLAGS =\1 -march=native -fomit-frame-pointer -fsanitize=address $(INCLUDES)|g' Makefile
#sed -i 's|^export CXXFLAGS =\(.*\) \$(INCLUDES)$|export CXXFLAGS =\1 -march=native -fomit-frame-pointer $(INCLUDES)|g' Makefile
# Add the path to possible locally-compiled included libraries
#CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" make || error_exit "${prog} build failed"
#INCLUDES="-I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" make || error_exit "${prog} build failed"
#sed -i 's|^export CXXFLAGS =\(.*\) \$(INCLUDES)$|export CXXFLAGS =\1 -fsanitize=address $(INCLUDES)|g' Makefile
make || error_exit "${prog} build failed"

ln -sf ${SOURCE}/bin/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"


)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
