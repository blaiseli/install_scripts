#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# (Not sure it works)

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="pigz"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.3.3" get_pigz.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.4"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue http://zlib.net/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
# Trying to optimize
sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
LDFLAGS="-Wl,-rpath,${HOME}/lib -I${HOME}/include -L${HOME}/lib" make test || error_exit "${prog} build failed"
ln -sf ${HOME}/src/${prog}-${version}/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"
ln -sf ${HOME}/src/${prog}-${version}/un${prog} ${HOME}/bin/. || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
