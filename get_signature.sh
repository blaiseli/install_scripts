#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}



mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf msp_sr_signature
hg clone https://testtoolshed.g2.bx.psu.edu/repos/drosofff/msp_sr_signature || error_exit "signature download failed"
cd msp_sr_signature
chmod +x signature.py
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd ${DIR}
ln -s ${HOME}/src/msp_sr_signature/*.py .

exit 0
