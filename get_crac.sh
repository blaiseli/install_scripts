#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="crac"
#version="1.5.0"
#version="2.0.0"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.4.0" get_crac.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.5.0"
fi


mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${program}-${version}*
# Download it
wget https://gforge.inria.fr/frs/download.php/latestfile/3502/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz || error_exit "${program} unpacking failed"
(
cd ${program}-${version}
# Trying to optimize
#sed -i 's/ -funroll-loops//' configure
#sed -i 's/ -funroll-loops//' src/libGkArrays/configure
#make clean
#./configure --prefix=${HOME} CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
./configure --prefix=${HOME} CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
#./configure --prefix=${HOME} CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
#./configure --prefix=${HOME} || error_exit "${program} config failed"
make || error_exit "${program} build failed"
make check || error_exit "${program} check failed"
make install || error_exit "${program} install failed"
)

if [[ ${1} ]]
then
    version_file="${1}"
else
    version_file="/dev/stdout"
fi

echo -e "${program}\t${version}" >> ${version_file}

exit 0
