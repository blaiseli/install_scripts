#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src
(
cd ${HOME}/src

#cython_version="0.22.1"
#rm -rf Cython-${cython_version}*
#wget --continue http://cython.org/release/Cython-${cython_version}.tar.gz || error_exit "Cython download failed"
#tar -xvzf Cython-${cython_version}.tar.gz
#cd Cython-${cython_version}
#CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "cython build failed"
#python setup.py install --user || error_exit "cython install failed"
#cd ..

prog="sickle"

if [ ! -e ${prog} ]
then
    git clone https://github.com/najoshi/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

make OPT="-O3 -march=native -fomit-frame-pointer" LDFLAGS="-Wl,-rpath,${HOME}/lib -I${HOME}/include -L${HOME}/lib" || error_exit "${prog} build failed"

ln -sf ${PWD}/${prog} ${HOME}/bin/${prog} || error_exit "${prog} install failed"


version=$(git log | head -1)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
