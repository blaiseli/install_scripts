#!/usr/bin/env bash
# To use after get_samtools.sh so that the programs can use the locally-compiled zlib
# Also depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="HTSeq"

# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="0.6.1p1" get_HTSeq.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="0.6.1p1"
fi


mkdir -p ${HOME}/src
(
cd ${HOME}/src
rm -rf ${program}-${version}*
#wget --continue --no-check-certificate https://pypi.python.org/packages/source/H/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
wget --continue --no-check-certificate https://files.pythonhosted.org/packages/source/H/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${program}\t${version}" >> ${version_file}

exit 0
