#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="MACS2"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.1.0.20150420" get_MACS2.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.1.0.20150420"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
rm -rf ${program}-${version}*
#wget --continue --no-check-certificate https://pypi.python.org/packages/source/M/${program}/${program}-${version}.1.tar.gz || error_exit "${program} download failed"
wget --continue --no-check-certificate https://files.pythonhosted.org/packages/source/M/${program}/${program}-${version}.1.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.1.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup_w_cython.py build || error_exit "${program} build failed"
python setup_w_cython.py install --user || error_exit "${program} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
