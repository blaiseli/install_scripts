#!/usr/bin/env bash
# sudo apt install flex

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src
(
cd ${HOME}/src

prog="ngs-sdk"
if [ ! -e ngs ]
then
    git clone https://github.com/ncbi/ngs.git || error_exit "${prog} download failed"
    cd ngs
else
    cd ngs
    git pull || error_exit "${prog} update failed"
fi

cd ${prog}

./configure --prefix=${HOME} || error_exit "${prog} configure failed"
make clean
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"

version=$(git log | head -1)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

(
cd ${HOME}/src

prog="ncbi-vdb"
if [ ! -e ${prog} ]
then
    git clone https://github.com/ncbi/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

./configure --prefix=${HOME} || error_exit "${prog} configure failed"
make clean
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"
export LD_LIBRARY_PATH=${HOME}/lib64:$LD_LIBRARY_PATH
export NCBI_VDB_LIBDIR=${HOME}/lib64
#echo "export LD_LIBRARY_PATH=${HOME}/lib64:$LD_LIBRARY_PATH" >> ${HOME}/.bashrc
#echo "export NCBI_VDB_LIBDIR=/home/severine.chambeyron/lib64" >> ${HOME}/.bashrc

version=$(git log | head -1)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

(
cd ${HOME}/src

prog="sra-tools"
if [ ! -e ${prog} ]
then
    git clone https://github.com/ncbi/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

./configure --prefix=${HOME} || error_exit "${prog} configure failed"
make clean
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"

version=$(git log | head -1)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
