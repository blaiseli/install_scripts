#!/usr/bin/env bash
# To use after get_samtools.sh so that the programs can use the locally-compiled zlib
# Also depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="pysam"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="0.8.1" get_pysam.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="0.10.0"
fi
samtools_version="1.3.1"
htslib_version=${samtools_version}

mkdir -p ${HOME}/src
(
cd ${HOME}/src
rm -rf ${prog}-${version}*
#wget --continue --no-check-certificate https://pypi.python.org/packages/source/p/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
#wget --continue --no-check-certificate https://pypi.io/packages/source/p/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
wget --continue --no-check-certificate https://files.pythonhosted.org/packages/source/p/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
#wget https://github.com/pysam-developers/pysam/archive/${version}.tar.gz
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
#/home/bli/.local/lib/python2.7/site-packages/pysam-0.9.1.4-py2.7-linux-x86_64.egg/pysam/cutils.so: undefined symbol: stdscr
#cp -rnu ../samtools-${samtools_version}/* samtools/.
#cp -rnu ../samtools-${samtools_version}/htslib-${htslib_version}/* htslib/.
#CFLAGS="-march=native" HTSLIB_LIBRARY_DIR="${HOME}/lib" HTSLIB_INCLUDE_DIR="${HOME}/include" python setup.py build
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${prog} build failed"
python setup.py install --user || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
