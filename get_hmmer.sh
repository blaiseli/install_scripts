#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="hmmer"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="3.2.1" get_hmmer.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="3.2.1"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue http://eddylab.org/software/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
# Trying to optimize
CFLAGS="-O3 -march=native -fomit-frame-pointer" ./configure || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make check || error_exit "${prog} test failed"
make install prefix=${HOME} || error_exit "${prog} install failed"
(
cd easel
make || error_exit "easel build failed"
make install prefix=${HOME}  || error_exit "easel install failed"
)
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
