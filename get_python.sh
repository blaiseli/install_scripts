#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="python"
progname="Python"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.7.12" get_python.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.7.12"
fi

# For some development versions, a release has to be specified
# For instance `VERSION="3.8.0" RELEASE="a4" get_python.sh`
if [[ ${RELEASE} ]]
then
    release="${RELEASE}"
else
    release=""
fi


# http://stackoverflow.com/questions/1210664/no-module-named-sqlite3
#sudo apt install libsqlite3-dev

#sudo apt install zlib1g-dev libffi-dev libssl-dev liblzma-dev

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${progname}-${version}*
# Download it
wget --continue https://www.python.org/ftp/${prog}/${version}/${progname}-${version}${release}.tar.xz || error_exit "${prog} download failed"
tar -xvJf ${progname}-${version}${release}.tar.xz || error_exit "${prog} unpacking failed"
(
cd ${progname}-${version}${release}
# It is possible to set PREFIX to use a custom prefix instead of the default ${HOME}
# For instance `VERSION="3.8.0" RELEASE="a2" PREFIX="${HOME}/python3.8" get_python.sh`
[ ${PREFIX} ] || PREFIX=${HOME}
export PYTHONPATH=${PREFIX}
#./configure --prefix=${PREFIX} CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${PREFIX}/include" LDFLAGS="-L${PREFIX}/lib -Wl,-rpath,${PREFIX}/lib" || error_exit "${prog} config failed"
if [[ ${LOCAL_LIBS} ]]
then
    ./configure --enable-shared --enable-optimizations --with-ensurepip=install --prefix=${PREFIX} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${PREFIX}/lib -Wl,-rpath,${PREFIX}/lib" || error_exit "${prog} config failed"
else
    ./configure --enable-shared --enable-optimizations --with-ensurepip=install --prefix=${PREFIX} CFLAGS="-O3 -march=native -fomit-frame-pointer" LDFLAGS="-L${PREFIX}/lib -Wl,-rpath,${PREFIX}/lib" || error_exit "${prog} config failed"
fi
#./configure --prefix=${PREFIX} --with-universal-archs=64-bit || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make install || error_exit "${prog} install failed"
distutils_cfg="${PREFIX}/lib/${prog}$(echo ${VERSION} | cut -d "." -f 1,2)/distutils/distutils.cfg"
echo "setting ${distutils_cfg}"
# TODO: This duplicates install section if already exists, which may cause problems.
echo '[install]' >> ${distutils_cfg} || error_exit "failing to set up ${distutils_cfg}"
echo "prefix=${PREFIX}" >> ${distutils_cfg}
echo "install-scripts=${PREFIX}/bin" >> ${distutils_cfg}
)

if [[ ${1} ]]
then
    version_file="${1}"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}${release}" >> ${version_file}

exit 0
