#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src
(
cd ${HOME}/src
rm -rf edirect*
wget "ftp.ncbi.nlm.nih.gov/entrez/entrezdirect/edirect.tar.gz"
tar -xvzf edirect.tar.gz
(
cd ${HOME}/src/edirect
wget "ftp.ncbi.nlm.nih.gov/entrez/entrezdirect/xtract.Linux.gz"
gunzip -f xtract.Linux.gz
chmod +x xtract.Linux
)
)
exit 0
