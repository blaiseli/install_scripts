#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# sudo apt-get install byacc

mkdir -p ${HOME}/src
cd ${HOME}/src

prog="bioawk"

if [ ! -e ${prog} ]
then
    git clone https://github.com/lh3/${prog}.git || error_exit "${prog} download failed"
    cd ${prog}
else
    cd ${prog}
    git pull || error_exit "${prog} update failed"
fi

CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" make || error_exit "${prog} build failed"

ln -sf ${HOME}/src/${prog}/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"

exit 0
