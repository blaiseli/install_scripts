#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


prog="dsk"
# https://github.com/GATB/dsk

mkdir -p ${HOME}/src
(
cd ${HOME}/src

if [ ! -e "${prog}" ]
then
    git clone --recursive https://github.com/GATB/${prog}.git || error_exit "${prog} download failed"
    cd "${prog}"
    version=$(git log | head -1)
else
    cd "${prog}"
    # Stash away modified Makefile, otherwise pull may fail
    # git stash
    git pull || error_exit "${prog} update failed"
    version=$(git log | head -1)
fi
SOURCE="${HOME}/src/${prog}"

cd ${SOURCE}
# sh INSTALL
# Prepare GATB sub-module
git submodule init
git submodule update

# Prepare directories:
rm -rf build
mkdir build

# Go in the 'build' directory
cd build

# Prepare the makefile
cmake .. || error_exit "${prog} makefile preparation failed"

# Run the newly created makefile:
make -j8 || error_exit "${prog} build failed"

# Go back at the installation root directory
cd ${SOURCE}

# run tests
echo "Running simple test..."
cd scripts 
. ./simple_test.sh
cd ${SOURCE}

ln -sf ${SOURCE}/build/bin/${prog} ${HOME}/bin/. || error_exit "${prog} install failed"

)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
