#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="hisat2"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.0.4" get_hisat2.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.0.4"
fi

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue ftp://ftp.ccb.jhu.edu/pub/infphilo/${prog}/downloads/${prog}-${version}-source.zip || error_exit "${prog} download failed"
unzip ${prog}-${version}-source.zip
cd ${prog}-${version}
mv Makefile Makefile.original
# Use native processor architecture for compiling 
sed 's/^RELEASE_FLAGS\(.*\)$/RELEASE_FLAGS\1 -march=native/g' Makefile.original > Makefile
make || error_exit "${prog} build failed"
# Install in home directory
mkdir -p ${HOME}/bin
for binary in $(ls -1 ${prog}* | grep -v debug)
#for binary in "${prog}" "${prog}-align-s" "${prog}-align-l" "${prog}-build" "${prog}-build-s" "${prog}-build-l" "${prog}-inspect" "${prog}-inspect-s" "${prog}-inspect-l"
do
    # Force link re-creation if they already exist
    ln -sf ${HOME}/src/${prog}-${version}/${binary} ${HOME}/bin/. || error_exit "${binary} install failed"
done
for script in $(ls -1 *.py | grep -v debug)
#for script in "extract_splice_sites.py" "extract_exons.py" "test_BRCA_genotyping.py" "simulate_reads.py" "test_HLA_genotyping.py" "genotype.py" "extract_snps_haplotypes_VCF.py" "extract_snps_haplotypes_UCSC.py" "extract_HLA_vars.py" "build_genotype_genome.py"
do
    # Force link re-creation if they already exist
    ln -sf ${HOME}/src/${prog}-${version}/${script} ${HOME}/bin/. || error_exit "${script} install failed"
done
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
