#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

#sudo apt-get install libncursesw5-dev

prog="htop"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.0.1" get_htop.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.0.1"
fi


mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue http://hisham.hm/${prog}/releases/${version}/${prog}-${version}.tar.gz \
    || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz \
    || error_exit "${prog} unpacking failed"
(
cd ${prog}-${version}
#./configure --prefix=${HOME} CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${prog} config failed"
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" \
    || error_exit "${prog} config failed"
#./configure --prefix=${HOME} --with-universal-archs=64-bit || error_exit "${prog} config failed"
make || error_exit "${prog} build failed"
make check || error_exit "${prog} check failed"
make install || error_exit "${prog} install failed"
)

if [[ ${1} ]]
then
    version_file="${1}"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
