#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="cufflinks"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="2.2.0" get_cufflinks.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="2.2.1"
fi

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget --continue http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/${prog}-${version}.Linux_x86_64.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.Linux_x86_64.tar.gz || error_exit "${prog} unpacking failed"
(
cd ${HOME}/bin
for binary in "cuffcompare" "cuffdiff" "cufflinks" "cuffmerge" "cuffnorm" "cuffquant" "gffread" "gtf_to_sam"
do
    ln -sf ${HOME}/src/${prog}-${version}.Linux_x86_64/${binary} .
done
)

if [[ ${1} ]]
then
    version_file="${1}"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
