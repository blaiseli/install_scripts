#!/usr/bin/env bash
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="artemis"
# It is possible to set the version in the `${VERSION}` environment variable to override the default.
# example: `VERSION="18.0.2" get_artemis.sh`
if [[ ${VERSION} ]]
then
    version="${VERSION}"
else
    version="18.0.2"
fi
prog_dir="artemis-unix-release-${version}"

mkdir -p ${HOME}/src
(
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}
# Download it
wget --continue https://github.com/sanger-pathogens/Artemis/releases/download/v${version}/artemis-unix-release-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf artemis-unix-release-${version}.tar.gz
# Force link re-creation if they already exist
ln -sf ${HOME}/src/${prog}/art ${HOME}/bin/. || error_exit "${prog} install failed"
)

if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
