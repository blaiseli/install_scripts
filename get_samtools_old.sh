#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="samtools"
version="1.1"
# Will the version always be the same?
htslib_version=${version}
# No
#version="1.2"
#htslib_version="1.2.1"


mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${prog}-${version}*
wget --continue http://downloads.sourceforge.net/project/${prog}/${prog}/${version}/${prog}-${version}.tar.bz2 || error_exit "${prog} download failed"
tar -xvjf ${prog}-${version}.tar.bz2
cd ${prog}-${version}/
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
sed -i 's|^LDFLAGS\(.*\)$|LDFLAGS\1 -Wl,-rpath,${HOME}/lib|g' Makefile
sed -i 's|^LDLIBS\(.*\)$|LDLIBS\1 -L${HOME}/lib|g' Makefile
cd htslib-${htslib_version}
sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
sed -i 's|^LDFLAGS\(.*\)$|LDFLAGS\1 -Wl,-rpath,${HOME}/lib|g' Makefile
sed -i 's|^LDLIBS\(.*\)$|LDLIBS\1 -L${HOME}/lib|g' Makefile
make || error_exit "htslib build failed"
# Install in home directory
make prefix=${HOME} install || error_exit "htslib install failed"
cd ..
make || error_exit "${prog} build failed"
# Install in home directory
make prefix=${HOME} install || error_exit "${prog} install failed"


if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
