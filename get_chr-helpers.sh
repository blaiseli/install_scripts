#!/usr/bin/env bash
# Depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="chr-helpers"

mkdir -p ${HOME}/src
cd ${HOME}/src

if [ ! -e ${program} ]
then
    git clone https://github.com/TheChymera/chr-helpers.git || error_exit "${program} download failed"
    cd ${program}
else
    cd ${program}
    git pull || error_exit "${program} update failed"
fi

CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"

exit 0
